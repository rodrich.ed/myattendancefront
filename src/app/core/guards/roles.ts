export enum Role {
  Asesor = 1,
  Coordinador = 2,
  Administrador = 3,
  Supervisor = 4
}

export namespace Rol {
  export function fromInt(s: number): Role | null{
    let  r: null = null;
    for (const item in Role) {
      // @ts-ignore
      if (Role[item.toString()] == s) {
        // @ts-ignore
        r = Role[item.toString()] as Role;
        break;
      }
    }
    return r;
  }


  export function getName(role: Role): string{
    let name = '';
    switch (role) {
      case Role.Asesor:
        name = 'Asesor';
        break;
      case Role.Coordinador:
        name = 'Coordinador';
        break;
      case Role.Administrador:
        name = 'Administrador';
        break;
      case Role.Supervisor:
        name = 'Supervisor';
        break;
      default:
        break;
    }
    return name;
  }
}

