import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Role } from './roles';
import { Location } from '@angular/common';
import {AuthService} from '@core/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router,
              private location: Location,
              private authService: AuthService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const roles: Role[] = route.data.roles || [];
    const users: number[] = route.data.extUsers || [];
    const currentUser = Number(localStorage.getItem('userIdfromServ'));
    if (this.authService.hasRolPermision(roles)){
      return true;
    } else {
      if(users.indexOf(currentUser)==-1){
        this.router.navigate(['/error/403']).then(r => {});
        return false;
      } else {
        return true;
      }
    }
    // not logged in so redirect to login page with the return url
    this.router.navigate(['/auth']).then(r =>{});
    return false;
  }

}
