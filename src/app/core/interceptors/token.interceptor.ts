import { Injectable, Injector} from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import {NavigationEnd, Router} from '@angular/router';
import { AuthService } from '@core/services/auth.service';
import { Observable } from 'rxjs';
import {filter, map} from 'rxjs/operators';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private inj: Injector, private router: Router,
              private authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map((event:any) => {
        if (event instanceof HttpResponse) {
           if(event.body.success==false) {
              if(event.body.error.code == 'INVALID_SESSIONID'){
                this.loginAccesVentor();
              }
            }
        }
        return event;
      })
    ) as any;
  }

  loginAccesVentor(): void {
    const username = localStorage.getItem('username');
    const assesKey = localStorage.getItem('assesKey');
    const userid = localStorage.getItem('userIdfromServ');
    /*this.router.navigate(['/auth'], {queryParams: { acceskey: assesKey, username: username, userid: userid, navigate: }})*/
    this.authService.logOperation(username==null?'cbarriga':username).subscribe(
      data => {
        this.authService.loginAccessKey(data.result.token, assesKey==null?'sdd':assesKey, username==null?'cbarriga':username).subscribe(
          response => {
            localStorage.setItem('sessionName', response.result.sessionName);
            localStorage.setItem('userId', response.result.userId);
            localStorage.setItem('username', String(username));
            localStorage.setItem('userIdfromServ', String(userid));
            setTimeout(function() {
              location.reload(true);
            }, 300);
          }, error => {
          });
      }, error => {
      });
  }

}
