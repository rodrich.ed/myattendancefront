import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from "rxjs";
import { IPageRequestX } from './models';
import {PaginationService} from "@core/services/pagination.service";

@Injectable({
  providedIn: 'root'
})
export class ConvocatoriaService {

  url: string = environment.apiMarvisur;
  constructor(private  _http: HttpClient) { }



  getConvocatoriaAll(): Observable<any> {
    return this._http.get<any>(this.url + 'obtenerconvocatorias').pipe();
  }
  postStudent(blog:any):Observable<any>{
    let json =JSON.stringify(blog);
    let params = json;
    let headers =new HttpHeaders().set('Content-Type','application/json');
    return this._http.post(this.url+'student/createStudent',params,{headers:headers}).pipe();
  }




}
