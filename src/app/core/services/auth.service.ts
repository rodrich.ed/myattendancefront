import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Md5} from 'ts-md5/dist/md5';
import {IUser} from '@models';
import {Role} from '@core/guards/roles';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url: string = environment.hostCRM;
  constructor(private  httpClient: HttpClient) { }

  logOperation(username: string): Observable<any>{
    let params= new HttpParams();
    params= params.set('operation', 'getchallenge');
    params= params.set('username', username);
    return this.httpClient.get<any>(this.url+'webservice.php', {params}).pipe();
  }

  loginAccessKey(tokenString: string, accessKey: string, username: string):Observable<any>{
    const md5 = new Md5();
    const tokenizer = String(md5.appendStr(tokenString + accessKey).end());
    const formdata: FormData = new FormData();
    formdata.append('accessKey', tokenizer);
    formdata.append('operation', 'login');
    formdata.append('username', username);
    return this.httpClient.post<any>(this.url+'webservice.php', formdata).pipe();
  }

  // @ts-ignore
  getRoleUser(): Role {
    let currentUser: IUser
    currentUser = JSON.parse(<string> localStorage.getItem('userObj'));
    switch(currentUser.role) {
      case 'H5':
        return Role.Asesor
      case 'H4':
        return Role.Coordinador
      case 'H10':
        return Role.Supervisor
      default:
        return Role.Administrador
    }
  }

  hasRolPermision(roles: Role[]): boolean {
    if (roles.length == 0) {
      return true;
    }
    const rolesSaved = this.getRoleUser();
    for (const rolRequest in roles) {
        if (roles[rolRequest] == rolesSaved) {
          return true;
        }
    }
    return false;
  }

}
