import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IResult} from '@models';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  url: string = environment.hostCRM;
  constructor(private  httpClient: HttpClient) { }

  getAllUsers():Observable<any> {
    let params= new HttpParams();
    params= params.set('operation', 'query');
    params= params.set('sessionName', <string>localStorage.getItem('sessionName'));
    params= params.set('query', 'SELECT * FROM Users where  id != 19x21 and  id != 19x14 and id != 19x19 and id != 19x27 and reports_to_id in (19x21,19x14,19x19,19x27);');
    return this.httpClient.get<any>(this.url+'webservice.php', {params}).pipe();
  }

  getReportUsers():Observable<IResult<any>> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let body = '';
    let sname= localStorage.getItem('sessionName');
    body = 'operation=' + 'get_report_data' + '&sessionName=' + sname + '&report_name=' + 'advisors_list';
    return this.httpClient.post<IResult<any>>(this.url+'webservice.php', body, {headers}).pipe();
  }


  getUsers():Observable<IResult<any>>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let body = '';
    let sname= localStorage.getItem('sessionName');
    body = 'operation=' + 'get_report_data' + '&sessionName=' + sname + '&report_name=' + 'sys_users_list';
    return this.httpClient.post<IResult<any>>(this.url+'webservice.php', body, {headers}).pipe();
  }


}
