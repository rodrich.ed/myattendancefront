import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from "rxjs";
import {PaginationService} from "@core/services/pagination.service";

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {

  url: string = environment.host;
  constructor(private  _http: HttpClient) { }

  postAttendance(attendance:any):Observable<any>{
    let json =JSON.stringify(attendance);
    let params = json;
    let headers =new HttpHeaders().set('Content-Type','application/json');
    return this._http.post(this.url+'attendance/createAttendance',params,{headers:headers}).pipe();
  }


}
