import { Injectable } from '@angular/core';
export interface Span {
  span: number;
}
interface SpanColumnContext {
  span: Span;
  spannedRow: Object;
}

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() {
  }

  diasHabiles(inicioDt: string, finDt: string) {
    const inicio = new Date(inicioDt); //Fecha inicial
    const fin = new Date(finDt); //Fecha final
    const timeDiff = Math.abs(fin.getTime() - inicio.getTime());
    const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) + 1; //Días entre las dos fechas
    let cuentaFinde = 0; //Número de Sábados y Domingos
    const array = new Array(diffDays);
    for (var i = 0; i < diffDays; i++) {
      if (inicio.getDay() == 0) {
        cuentaFinde++;
      }
      inicio.setDate(inicio.getDate() + 1);
    }
    return {diashab: diffDays - cuentaFinde, domingos: cuentaFinde, difDias: diffDays};
  }

  diasEnteros(inicioDt: string, finDt: string) {
    const inicio = new Date(inicioDt); //Fecha inicial
    const fin = new Date(finDt); //Fecha final
    const timeDiff = Math.abs(fin.getTime() - inicio.getTime());
    const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); //Días entre las dos fechas
    return diffDays;
  }


  converDate(fecha: string): Date{
    let dtstr = fecha.split(' ');
    let str = dtstr[0].split('-');
    let cal = new Date(fecha);
    return cal;
  }

  getLetters(day: Date, type: string): string {
    let dias = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
    let meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    if (type === 'dia') {
      return dias[day.getDay()];
    } else if (type === 'mes') {
      return meses[day.getMonth()];
    }
    return 'No es una fecha';
  }


  compute(data: Object[], columns: string[]): Array<Span[]> {
    const spans: Array<Span[]> = this.initSpans(columns);
    const spanColumnContexts: SpanColumnContext[] = new Array(columns.length);
    for (const row of data) {
      for (let iCol = 0; iCol < columns.length; iCol++) {
        const column = columns[iCol];
        const spanColumnContext = spanColumnContexts[iCol];
        // @ts-ignore
        if (spanColumnContext && spanColumnContext.spannedRow[column] === row[column]) {
          ++spanColumnContext.span.span;
          spans[iCol].push({ span: 0 });
        } else {
          const span = { span: 1 };
          spanColumnContexts[iCol] = { span: span, spannedRow: row };
          spans[iCol].push( span );
          spanColumnContexts.slice(iCol + 1).forEach(c => c.spannedRow = {});
        }
      }
    }
    return spans;
  }



  private initSpans(columns: string[]): Array<Span[]> {
    const spans: Array<Span[]> = [];
    columns.forEach(p => spans.push([]));
    return spans;
  }


}




