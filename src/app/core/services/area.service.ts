import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  url: string = environment.host;
  constructor(private  _http: HttpClient) { }
  getArea():Observable<any>{
    return this._http.get(this.url+'area/getArea');
  }
}
