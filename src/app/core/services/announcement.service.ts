import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {ITeacher} from "@models";
import {map} from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class AnnouncementService {


  url: string = environment.host;
  // url="http://192.168.0.183:8000/v1/"
  constructor(private  _http: HttpClient) { }

  getAnnouncement():Observable<any>{
    return this._http.get(this.url+'announcement/getAnnouncement');
  }
  postAnnouncement(data:any):Observable<any>{
    let json =JSON.stringify(data);
    let params = json;
    let headers =new HttpHeaders().set('Content-Type','application/json');
    return this._http.post(this.url+'announcement/createAnnouncement',params,{headers:headers}).pipe();

  }
  deleteAnnouncement(idAnnouncement:any):Observable<any>{
    return this._http.delete(this.url+'reports/delete/'+idAnnouncement);
  }
  getAnnouncementId(id:any):Observable<any>{
    return this._http.get(this.url+'reports/find/'+id);
  }
  updateAnnouncement(blog:any,idBlog:any):Observable<any>{
    let json =JSON.stringify(blog);
    let params = json;
    let headers =new HttpHeaders().set('Content-Type','application/json');
    return this._http.put(this.url+'reports/update/'+idBlog,params,{headers:headers}).pipe();
  }

}
