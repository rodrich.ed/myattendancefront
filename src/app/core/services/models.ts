export interface IResult<T> {
  success: string;
  result: T[];
}

export interface IModel {
  id?: number;
}

export interface IModelTimestamp extends IModel {
  deleted_at?: Date;
  updated_at?: Date;
  created_at?: Date;
}

export class Creator {
  static page<I extends IModel>(): {
    per_page: number; total: number; data: any[]; last_page: number; next_page_url: number;
    from: number; to: number; prev_page_url: number; current_page: number } {
    return {
      total: 0,
      per_page: 10,
      current_page: 0,
      last_page: 0,
      next_page_url: 0,
      prev_page_url: 0,
      from: 0,
      to: 0,
      data: [],
    };
  }
}

export interface IPageRequestX {
  sort: string|null;
  sortDirection: string|null;
  limit: number;
  page: number;
}

export interface IPageable<T extends IModel> {
  total: number;
  per_page: number;
  current_page: number;
  last_page: number;
  next_page_url: string;
  prev_page_url: string;
  from: number;
  to: number;
  data: T[];
}

export interface IUser{
  id: string;
  username: string;
  role: string;
  name: string;
  sex?: string;
}

export interface ITeacher{
  recordId: string;
  nombre: string;
}

