import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPageRequestX } from './models';

export class PaginationService {
  static getPageParams(page: IPageRequestX): HttpParams{
    let params = new HttpParams();
    if (page != null) {
      params = params.set('limit', page.limit + '').set('page', page.page + '');

      if (page.sortDirection != null){
        params = params.set('sort_direction', page.sortDirection);
      }
      if (page.sort != null){
        params = params.set('sort', page.sort );
      }
    }
    return params;
  }
}
