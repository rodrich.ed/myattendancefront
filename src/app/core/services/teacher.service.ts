import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ITeacher} from "@models";
import {map} from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class TeacherService {


  url: string = environment.host;
  constructor(private  _http: HttpClient) { }

  getTeachers():Observable<ITeacher[]>{
    return this._http.get<ITeacher[]>(this.url+'docentes/getdocentes').pipe(
      map( (resp:any[]) =>
        resp.map( teacher => ({ recordId: teacher.recordId, nombre: teacher.grado+' '+teacher.nombre +' '+teacher.apellido } )
        )
      )
    );
  }
}
