import {IModelTimestamp} from "@models";

export interface IComment extends IModelTimestamp{
  comment:    string;
  name:       string;
  email:      string;
  validate:   number;
  blog_id:    number;
}
