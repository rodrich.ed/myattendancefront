import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AttendanceService} from "@core/services/attendance.service";
import {MatDialog} from "@angular/material/dialog";
import {AlertDialogComponent} from "@shared/utils/alert-dialog/alert-dialog.component";

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.scss']
})
export class AttendanceComponent implements OnInit {
  dialogRefAlert: any;
  form!: FormGroup;
  constructor(private  _attendanceService:AttendanceService,public dialog: MatDialog) {
    this.form = new FormGroup({
      'dni': new FormControl('', [Validators.required])
    });
  }

  ngOnInit(): void {
      // this.chargeHour();

  }
  saveAttendance(){
    let newAttendance = {
      "dni": this.form.controls['dni'].value,
    }
    // console.log(newAttendance);
    this._attendanceService.postAttendance(newAttendance).subscribe(data =>{
      console.log(data.message,"mensaje");
      if (data.message=='accept'){
        this.openDialog("Se Ingresó correctamente la asistencia","success");
        this.form.reset();
      }else{
        this.openDialog("Se produjo un error en el ingreso de asistencia","error");
      }
      // console.log(data);
    },error => {
      this.openDialog("Se produjo un error en el ingreso de asistencia","error");

    });
    // console.log(newBlog);
  }
  openDialog(labelAnswer: string, type: string): void {
    this.dialogRefAlert = this.dialog.open(AlertDialogComponent, {
      width: '40vh',
      data: {labelAnswer: labelAnswer, type: type}
    });
  }

}
