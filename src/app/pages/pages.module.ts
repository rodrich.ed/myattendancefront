import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { Error403Component } from './error403/error403.component';
import {FlexLayoutModule} from '@angular/flex-layout';


@NgModule({
  declarations: [
    Error403Component
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    FlexLayoutModule
  ]
})
export class PagesModule { }
