import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import {MatMenuModule} from "@angular/material/menu";
import {MatSidenavModule} from "@angular/material/sidenav";
import { ContentModule } from './content/content.module';
import { LayoutsModule } from '@shared/layouts/layouts.module';
import { UtilsModule } from '@shared/utils/utils.module';
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatIconModule} from '@angular/material/icon';
import {SideNavService} from '@core/layout-services/side-nav.service';


@NgModule({
  declarations: [
    MainComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    FlexLayoutModule,
    MatMenuModule,
    MatSidenavModule,
    UtilsModule,
    LayoutsModule,
    ContentModule,
    MatIconModule
  ],
  providers: [SideNavService],
})
export class MainModule { }
