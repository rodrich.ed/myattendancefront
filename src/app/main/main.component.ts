import { MediaMatcher } from '@angular/cdk/layout';
import {AfterContentInit, AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Subscriber, Subscription} from 'rxjs';
import {elementAt, filter} from 'rxjs/operators';
import {SideNavService} from '@core/layout-services/side-nav.service';
import {IUser} from '@models';
import {navigation} from '@shared/navigation/navigation';
import {AuthService} from '@core/services/auth.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit {
  flagOpenToolbar = false;
  currentUrl = '';
  @ViewChild('sidenav') public sidenav!: MatSidenav;
  currentUser!: IUser;
  navigations = navigation;
  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
              private sidenavService: SideNavService,
              private router: Router,
              private route: ActivatedRoute) {
    this.currentUser = JSON.parse(<string>localStorage.getItem('userObj'));
    // @ts-ignore
    router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((event: NavigationEnd) => {
      this.currentUrl = event.url;
    });
  }
  ngOnInit(): void {}

  ngAfterViewInit(): void {
    const thos = this;
    setTimeout(function() {
      thos.sidenavService.sideNavToggleSubject.subscribe(()=> {
        thos.sidenav.toggle().then(r => {});
      });
    },200);
    setTimeout(function() {
      thos.sidenav.toggle().then(r => {});
    },200);
  }

  isAsesor(){
    if(this.currentUser.role=='H5'){
      return true;
    }
    return false;
  }

  hasRole(element: any, i: number, j: number): boolean {
    // const currentRole = this.authService.getRoleUser();
    const currentUser = Number(localStorage.getItem('userIdfromServ'));
    const permisionRoles = element.role;
    const exterUser = element.extUsers;
    // if(permisionRoles.indexOf(currentRole)!= -1){
    //   this.navigations[i].role = 'none';
    //   this.navigations[i].children[j].role = 'none';
    //   return true;
    // } else {
    //   if(exterUser.indexOf(currentUser) != -1){
    //     this.navigations[i].role = 'none';
    //     this.navigations[i].children[j].role = 'none';
    //     return true;
    //   } else {
    //     return false;
    //   }
    // }
    return true;
  }



  // changeUrl(getUrl:string): boolean {
  //   return  getUrl == this.currentUrl;
  // }

}
