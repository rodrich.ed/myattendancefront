  import {Component, forwardRef, Input, OnInit} from '@angular/core';
// @ts-ignore
import * as customBuilder from './build/ckEditor';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
@Component({
  selector: 'app-ckeditor',
  templateUrl: './ckeditor.component.html',
  styleUrls: ['./ckeditor.component.scss'],
  providers:[ {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() =>CkeditorComponent),
    multi: true
  }]
})
export class CkeditorComponent implements OnInit {
  public Editor = customBuilder;
  @Input() onlyView="si";
  public config_editor:any;

  @Input() readonly  = false;
  private _value: string = '';
  onChange(v:any) {
  }
  get value() {
    return this._value;
  }

  set value(v: string) {
    if (v !== this._value) {
      this._value = v;
      this.onChange(v);
    }
  }
  onTouch() { }
  writeValue(obj: any): void {
    this._value = obj;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
  constructor() {
    //
    //this.Editor.config.title=false;

  }

  ngOnInit(): void {

    if (this.onlyView=="si"){
      this.config_editor={
        removePlugins: ['Title'],
        image:{
        }
      };

    }else{
      this.config_editor= {
        toolbar: {
          items: [
            'heading', '|','italic','bold','underline',
            'fontfamily', 'fontsize',
            'alignment',
            'fontColor', 'fontBackgroundColor','|',
            'strikethrough', 'underline', 'subscript', 'superscript', '|',
            'link', '|',
            'outdent', 'indent', '|',
            'bulletedList', 'numberedList', 'todoList',
            'code', 'codeBlock', '|',
            'insertTable', '|',
            'imageUpload', 'blockQuote', '|','insertImage','mediaEmbed',
            'todoList'
            ,
            'undo', 'redo',
          ],
           shouldNotGroupWhenFull: true,

        },
        image: {
          // Configure the available styles.
          styles: [
            'alignLeft', 'alignCenter', 'alignRight'
          ],

          // Configure the available image resize options.
          resizeOptions: [
            {
              name: 'resizeImage:original',
              label: 'Original',
              value: null
            },
            {
              name: 'resizeImage:50',
              label: '25%',
              value: '25'
            },
            {
              name: 'resizeImage:50',
              label: '50%',
              value: '50'
            },
            {
              name: 'resizeImage:75',
              label: '75%',
              value: '75'
            }
          ],

          // You need to configure the image toolbar, too, so it shows the new style
          // buttons as well as the resize buttons.
          toolbar: [
            'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
            '|',
            'ImageResize',
            '|',
            'LinkImage',
            '|',
            'imageTextAlternative'
          ]
        },
        fontSize: {
          options: [
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            20,
            19,
            20,
            30,
            40
          ]
        },
        fontColor: {
          colors: [
            {
              color: 'hsl(0, 0%, 0%)',
              label: 'Black'
            },
            {
              color: 'hsl(0, 0%, 30%)',
              label: 'Dim grey'
            },
            {
              color: 'hsl(0, 0%, 60%)',
              label: 'Grey'
            },
            {
              color: 'hsl(0, 0%, 90%)',
              label: 'Light grey'
            },
            {
              color: 'hsl(0, 0%, 100%)',
              label: 'White',
              hasBorder: true
            },
            '#FF0F00','#FF4600','#FFD100','#E0FF00','#64FF00','#00FF04','#00FF83','#00FFE8',
            '#00C1FF','#0070FF','#0017FF','#6800FF','#B600FF','#FF00EC','#FF00AA','#FF0064',
            '#FF0000','#EC7063','#E8D997','#F3F5F7','#CC0064','#DBCF8A','#7D8191BF','#9B004C','#A81258','#891e4e'
            // ...
          ],


        },
        fontBackgroundColor: {
          colors: [
            {
              color: 'hsl(0, 0%, 0%)',
              label: 'Black'
            },
            {
              color: 'hsl(0, 0%, 30%)',
              label: 'Dim grey'
            },
            {
              color: 'hsl(0, 0%, 60%)',
              label: 'Grey'
            },
            {
              color: 'hsl(0, 0%, 90%)',
              label: 'Light grey'
            },
            {
              color: 'hsl(0, 0%, 100%)',
              label: 'White',
              hasBorder: true
            },
            '#EC5C5C','#EC7D5C','#ECA05C','#ECCD5C','#C7EC5C','#88EC5C','#5CEC81','#5CECCB',
            '#5CABEC','#5C78EC','#6D5CEC','#A25CEC','#D45CEC','#EC5CB3','#EC5C76','#EC5C63'
            // ...
          ]
        },
        fontFamily: {
          options: [
            'default',
            'Arial, Helvetica, sans-serif',
            'Courier New, Courier, monospace',
            'Georgia, serif',
            'Lucida Sans Unicode, Lucida Grande, sans-serif',
            'Tahoma, Geneva, sans-serif',
            'Times New Roman, Times, serif',
            'Trebuchet MS, Helvetica, sans-serif',
            'Verdana, Geneva, sans-serif'
          ]
        },
        removePlugins: ['Title'],


        language: 'en'
      };

    }
  }
  clickboton():void{
    console.log(this.value);

  }
  onReady(editor:any) {
    if (editor.model.schema.isRegistered('image')) {
      editor.model.schema.extend('image', { allowAttributes: 'blockIndent' });
    }
  }
}
