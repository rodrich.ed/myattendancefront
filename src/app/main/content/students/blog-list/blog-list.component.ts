import {Component, OnInit, ViewChild} from '@angular/core';
import {ConvocatoriaService} from "@core/services/convocatoria.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {ActivatedRoute} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {AlertDialogComponent} from "@shared/utils/alert-dialog/alert-dialog.component";
import {FormControl} from "@angular/forms";
import { forkJoin } from 'rxjs/internal/observable/forkJoin';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss'],
  providers: [ ConvocatoriaService ]
})
export class BlogListComponent implements OnInit {
  students: any[] = [];
  blogs: any[] = [];
  displayedColumns: string[] = ['nombre','code','fechainicio','categoria','tipo','ver'];
  dataSource:any;
  //Code for blogs.
  // displayedColumnsBlog: string[] = ['titulo','likes','interacciones','popular','ver','edit','delete'];
  dataSourceBlog:any;
  dialogRefAlert: any;
  filterNameFC = new FormControl();
  idProduct:any;
  dataBlog = [
    {'id':'1','titulo':'TITULO DE BLOCK EJEMPLO','programa':'MINERIA 01','likes':'12312','interacciones':'2321','tags':'#asdasd'},
    {'id':'2','titulo':'TITULO DE BLOCK EJEMPLO2','programa':'MINERIA 01','likes':'12312','interacciones':'2321','tags':'#asdasd'}
  ];

  /*** Variables de Filtros ***/
    nombreProducto = new FormControl();
    nombreCCosto   = new FormControl();
    idCategorias   = new FormControl();
    idTipos        = new FormControl();
    idCategoriasList:any[] = [];
    idTiposList     :any[] = [];
    pageIndex:any = 1;
    pageSize:any = 5;
    total:number = 10;
    sortFilter:string|null = null;
    sortDirectionFilter:string|null = null;

    pageIndexBlog:any = 1;
    pageSizeBlog:any = 10;
    totalBlog:number = 0;
    sortFilterBlog:string|null = null;
    sortDirectionFilterBlog:string|null = null;

  // @ViewChild('paginatorProduct') paginatorProduct !: MatPaginator;
  // @ViewChild('paginatorBlogs')   paginatorBlogs   !: MatPaginator;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  /*** Fin de Variables ***/

  constructor(private  _convocatoriaService: ConvocatoriaService, private router:ActivatedRoute, public dialog: MatDialog) {

  }

  ngOnInit(): void {

      this._convocatoriaService.getConvocatoriaAll().subscribe(data=>{
         this.students=data.convocatorias;
         this.dataSource = new MatTableDataSource<any>(this.students);
        console.log(data.convocatorias);

      },error => {

      });

  }

  ngAfterViewInit(): void {
    // this.paginatorProduct.page
    //   .subscribe((data)=>{
    //     this.pageIndex = this.paginatorProduct.pageIndex + 1;
    //     this.pageSize = this.paginatorProduct.pageSize;
    //     this.loadProducts();
    //   });
    //
    // this.paginatorBlogs.page
    //   .subscribe((data)=>{
    //     this.pageIndexBlog = this.paginatorBlogs.pageIndex + 1;
    //     this.pageSizeBlog = this.paginatorBlogs.pageSize;
    //     //this.loadProducts();
    //   });
  }

  loadProducts(): void {
    this._convocatoriaService.getConvocatoriaAll().subscribe(data=>{
      this.students=data.convocatorias;
      this.dataSource = new MatTableDataSource<any>(this.students);
      console.log(data.convocatorias);

    },error => {

    });
  }

  limpiar() {
    this.nombreProducto.reset();
    this.nombreCCosto.reset();
    this.idTipos.reset();
    this.idCategorias.reset();
    this.loadProducts();
  }

  openInfo(data:any):void {

  }

  deleteBlog(blog:any){
  }


  openDialog(labelAnswer: string, type: string): void {
    this.dialogRefAlert = this.dialog.open(AlertDialogComponent, {
      width: '40vh',
      data: {labelAnswer: labelAnswer, type: type}
    });
  }

}
