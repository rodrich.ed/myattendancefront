import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogAdminRoutingModule } from './blog-admin-routing.module';
import { BlogListComponent } from './blog-list/blog-list.component';
import { CreateBlogComponent } from './create-blog/create-blog.component';
import { CkeditorComponent } from './ckeditor/ckeditor.component';
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatButtonModule} from "@angular/material/button";
import {MatSelectModule} from "@angular/material/select";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatInputModule} from "@angular/material/input";
import {MatChipsModule} from "@angular/material/chips";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatTabsModule} from "@angular/material/tabs";


@NgModule({
  declarations: [
    BlogListComponent,
    CreateBlogComponent,
    CkeditorComponent
  ],
    imports: [
        CommonModule,
        BlogAdminRoutingModule,
        MatIconModule,
        CKEditorModule,
        FormsModule,
        MatTableModule,
        MatIconModule,
        MatPaginatorModule,
        MatButtonModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        MatInputModule,
        MatChipsModule,
        FlexLayoutModule,
        MatExpansionModule,
        MatTabsModule
    ]
})
export class BlogAdminModule { }
