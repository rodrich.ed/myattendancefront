import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BlogListComponent} from "./blog-list/blog-list.component";
import {CreateBlogComponent} from "./create-blog/create-blog.component";
import {AuthGuard} from "@core/guards/auth.guard";

const routes: Routes = [
  {
    path: '',
    component: BlogListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'create',
    component: CreateBlogComponent,
    canActivate: [AuthGuard]
  },



  {path: '**', redirectTo: 'admin'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogAdminRoutingModule { }
