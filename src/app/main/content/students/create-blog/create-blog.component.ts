import {Component, ElementRef, forwardRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, NG_VALUE_ACCESSOR, Validators} from "@angular/forms";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {Observable} from "rxjs";
import {ITeacher} from "@models";
import {TeacherService} from "@core/services/teacher.service";
import {map, startWith} from "rxjs/operators";
import {MatChipInputEvent} from "@angular/material/chips";
import {MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";
import {ActivatedRoute, Router} from "@angular/router";
import {ConvocatoriaService} from "@core/services/convocatoria.service";
import {AlertDialogComponent} from "@shared/utils/alert-dialog/alert-dialog.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-create-blog',
  templateUrl: './create-blog.component.html',
  styleUrls: ['./create-blog.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CreateBlogComponent),
      multi: true
    }
  ]
})
export class CreateBlogComponent implements OnInit {
  form!: FormGroup;
  borderInvalid = "";
  private _value: string = '';
  onChange(v:any) {
  }
  get value() {
    return this._value;
  }
  set value(v: string) {
    if (v !== this._value) {
      this._value = v;
      this.onChange(v);
    }
  }
  onTouch() { }
  writeValue(obj: any): void {
    this._value = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
  //config para el editor de texto.
  onlyView="no";
  dialogRefAlert: any;
  titleComponent = "";
  dataJsonBlog:any;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  fruitCtrl = new FormControl();

  filteredFruits!: Observable<string[]>;
  tags: string[] = [];
  idProduct:any;
  idBlog:any;
  options!: any[];

  likesUpdate:any;
  interactionsUpdate:any;
  productIdUpdate:any;

  @ViewChild('fruitInput') fruitInput!: ElementRef<HTMLInputElement>;
  constructor(private _teacherService: TeacherService, private router:ActivatedRoute, private _blogService: ConvocatoriaService, public dialog: MatDialog, private router1: Router ) {

    this.form = new FormGroup({
      'name': new FormControl('', [Validators.required]),
      'lastname': new FormControl('', [Validators.required]),
      'dni': new FormControl('', [Validators.required]),
      'email': new FormControl('', [Validators.required]),
      'grade': new FormControl('', [Validators.required]),
    });
  }

  myControl = new FormControl();

  filteredOptions!: Observable<ITeacher[]>;

  ngOnInit(): void {
    this.loadTeachers();
    // this.myControl.value.recordId =null;
  }
  private _filter(value: any): ITeacher[] {
    let filterValue = '';

    if( value.nombre){
      filterValue = value.nombre.toLowerCase();
    } else {
      filterValue = value.toLowerCase();
    }

    return this.options.filter(option => option.nombre.toLowerCase().includes(filterValue));
  }
  /*
   *cargar data de docentes.
   */
  loadTeachers():void{
    if(this.idBlog >0){
      this.loadDataBlog();
    }

    this._teacherService.getTeachers().subscribe(data => {
      this.options=data;
      this.filteredOptions = this.myControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value))
      );

    }, error => {
      console.log(error);
    });
  }
  /*
  *capturar y guardar data del blog.
   */
  saveBlog():void{
    // console.log("entro");

     let newBlog = {

       "name": this.form.controls['name'].value,
       "lastname": this.form.controls['lastname'].value,
       "email": this.form.controls['email'].value,
       "dni": this.form.controls['dni'].value,
       "grade": this.form.controls['grade'].value,
       "state": 1,


   }
   console.log(newBlog);
    this._blogService.postStudent(newBlog).subscribe(data=>{
      if(data.message=='accept'){
        this.router1.navigate(['/v1/blog/admin/']);
      }
    },error => {
    });

  }

  cleanFields(){
    this.form.controls['name'].setValue("");
    this.form.controls['lastname'].setValue("");
    this.form.controls['email'].setValue("");
    this.form.controls['dni'].setValue("");
    this.form.controls['grade'].setValue("");

  }
  openDialog(labelAnswer: string, type: string): void {
    this.dialogRefAlert = this.dialog.open(AlertDialogComponent, {
      width: '40vh',
      data: {labelAnswer: labelAnswer, type: type}
    });
  }

  displayFn(teacher?: ITeacher): any {
    return teacher ? teacher.nombre : undefined;
  }
  /*
  * metodo para capturar el teacher seleccionado en el autocompletado.
  */
  /*
    * metodo para cargar el blog si es para editar.
  */
  loadDataBlog(){
    // this._blogService.getBlogId(this.idBlog).subscribe(data =>{
    //   this.form.controls['title'].setValue(data.title);
    //   this.form.controls['image_web'].setValue(data.image_web);
    //   this.form.controls['short'].setValue(data.short_description);
    //   this.likesUpdate = data.likes;
    //   this.productIdUpdate = data.product_id;
    //   this.interactionsUpdate = data.interactions;
    //   this.tags=data.tags.split(",");
    //   // this.idProduct=data.product_id;
    //
    //   this.value=data.descriptions;
    //   this.myControl.setValue(this.options.find(x=> x.recordId == data.teacher_id));
    // },error=>{});
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    // Add our fruit
    if (value) {
      this.tags.push(value);
    }

    // Clear the input value
    event.chipInput!.clear();

    this.fruitCtrl.setValue(null);
  }

  remove(fruit: string): void {
    const index = this.tags.indexOf(fruit);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }
  selected(event: MatAutocompleteSelectedEvent): void {
    this.tags.push(event.option.viewValue);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
  }
  /*
  * metodo para registrar e insertar una imagen en el ckeditor.
  */
  onReady(editor:any) {
    if (editor.model.schema.isRegistered('image')) {
      editor.model.schema.extend('image', { allowAttributes: 'blockIndent' });
    }
  }
}
