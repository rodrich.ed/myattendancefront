import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {AnnouncementService} from "@core/services/announcement.service";
import {AlertDialogComponent} from "@shared/utils/alert-dialog/alert-dialog.component";
import {MatDialog} from "@angular/material/dialog";



const ELEMENT_DATA: any = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Title } from '@angular/platform-browser';
import {ConvocatoriaService} from "@core/services/convocatoria.service";
import {MatSort} from "@angular/material/sort";
export interface UserData {
  cargo: string;
  code: string;
  conocimientos: string;
  estado: string;
  estudios: string;
  fecha: string;
  funciones: string;
  lugar: string;
  sueldo: string;
  tipo: string;
  vacantes: string;
}
@Component({
  selector: 'app-list-announcement',
  templateUrl: 'list-announcement.component.html',
  styleUrls: ['list-announcement.component.scss']
})
export class ListAnnouncementComponent implements AfterViewInit {
  displayedColumns: string[] = ['id', 'name', 'progress', 'fruit'];

  dataSource!: MatTableDataSource<UserData>;
  resultsLength = 0;
  data:UserData[] = [];
  dialogRefAlert: any;
  panelOpenState = false;
  // displayedColumns: string[] = ['id','area', 'title','descriptions','requires','link','quantity','place','schedule','state','ver'];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private formBuilder: FormBuilder,
              private _convocatoriaService: ConvocatoriaService,
              public dialog: MatDialog) {
    // this.addTag();
  // this.loadData()



  }

  ngOnInit(){
    this.loadData()
  }
  loadData(){
    this._convocatoriaService.getConvocatoriaAll().subscribe(
      (events) => {

        this.dataSource = new MatTableDataSource(  events.convocatorias);
      }
    )

    // this._convocatoriaService.getConvocatoriaAll().subscribe(data=>{
    //   // this.students=data.convocatorias;
    //   // this.resultsLength = 8;
    //    this.data = data.convocatorias;
    //
    //   this.dataSource = new MatTableDataSource(this.data);
    //
    // },error => {
    //
    // });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  openDialog(labelAnswer: string, type: string): void {
    this.dialogRefAlert = this.dialog.open(AlertDialogComponent, {
      width: '40vh',
      data: {labelAnswer: labelAnswer, type: type}
    });
  }

  searchAttendance(){

  }
}
