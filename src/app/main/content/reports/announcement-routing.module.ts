import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "@core/guards/auth.guard";
import {ListAnnouncementComponent} from "./list-announcement/list-announcement.component";

const routes: Routes = [
  {
    path: '',
    component: ListAnnouncementComponent,
    canActivate: [AuthGuard]
  },
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnnouncementRoutingModule { }
