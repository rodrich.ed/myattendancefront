import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main.component';

const routes: Routes = [
  { path: '',
    component: MainComponent,
    children: [
      {path: 'alumnos', loadChildren: () => import('./content/students/blog-admin.module').then(m => m.BlogAdminModule)},
      {path: 'reportes', loadChildren: () => import('./content/reports/announcement.module').then(m => m.AnnouncementModule)}

    ]},
  {path: '**', redirectTo: 'blog'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
