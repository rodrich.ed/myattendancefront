import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SecurityComponent } from './security.component';
import {FlexModule} from '@angular/flex-layout';



@NgModule({
  declarations: [
    SecurityComponent
  ],
  imports: [
    CommonModule,
    FlexModule
  ]
})
export class SecurityModule { }
