import { Component, OnInit } from '@angular/core';
import {AuthService} from '../core/services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import users from './user-db.json';
import {IUser} from '@models';
import {map, startWith} from 'rxjs/operators';
// import {SettingsService} from '@core/services/settings.service';
@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.scss']
})
export class SecurityComponent implements OnInit {
  usersBD: IUser[] = [];
  accesKey: string | null = '';
  username: string | null = '';
  userid: string | null = '';
  navigate: string | null = '';
  constructor(private authService:AuthService,
              private router: Router,
              // private settingsService: SettingsService,
              private route:ActivatedRoute) {
    this.getUser();
    this.route.queryParamMap.subscribe(
      data => {
        this.accesKey = data.get('acceskey');
        this.username = data.get('username');
        this.userid = data.get('userid');
        this.navigate = data.get('navigate');
        const thos = this;
        setTimeout(function() {
          thos.loginAccesVentor();
        }, 300);
      });
  }

  ngOnInit(): void {
  }

  getUser(){
    // this.settingsService.getUsers().subscribe(
    //   data => {
    //     data.result.forEach(x => {
    //       let user: IUser =  {
    //         id: x.id.split('19x')[1],
    //         name: x.fullname,
    //         role: x.roleid.split('19x')[1],
    //         username: x.user_name
    //       }
    //       this.usersBD.push(user);
    //     });
    //   }
    // );
  }

  loginAccesVentor(): void {
    this.authService.logOperation(this.username==null?'cbarriga':this.username).subscribe(
      data => {
        this.authService.loginAccessKey(data.result.token, this.accesKey==null?'sdd':this.accesKey, this.username==null?'cbarriga':this.username).subscribe(
          response => {
            localStorage.setItem('sessionName', response.result.sessionName);
            localStorage.setItem('assesKey', <string>this.accesKey);
            localStorage.setItem('userId', response.result.userId);
            localStorage.setItem('username', String(this.username));
            localStorage.setItem('userIdfromServ', String(this.userid));
            if (this.usersBD.find(x=> x.id == String(this.userid))) {
              localStorage.setItem('userObj', JSON.stringify(this.usersBD.find(x=> x.id == String(this.userid))));
              this.navigateReports();
            }
          }, error => {
          });
      }, error => {
      });
  }

  navigateReports(){
    if (this.navigate === 'actividades') {
      this.router.navigate(['/v1/reportes/actividades']).then(r => {});
    } else if (this.navigate === 'asignacion-cierre') {
      this.router.navigate(['/v1/reportes/asignacion-cierre']).then(r => {});
    } else if (this.navigate === 'cambio-fase') {
      this.router.navigate(['/v1/reportes/cambio-fase']).then(r => {});
    }
    else if (this.navigate === 'base-atrasada') {
      this.router.navigate(['/v1/reportes/atrasada']).then(r => {});
    }
  }

}
