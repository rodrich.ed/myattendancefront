import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {UtilsModule} from '@shared/utils/utils.module';
import {LayoutsModule} from '@shared/layouts/layouts.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {SecurityModule} from './security/security.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import {SideNavService} from '@core/layout-services/side-nav.service';
import {TokenInterceptor} from '@core/interceptors/token.interceptor';
import { AttendanceComponent } from './attendance/attendance.component';
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    AppComponent,
    AttendanceComponent

  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        UtilsModule,
        LayoutsModule,
        HttpClientModule,
        SecurityModule,
        MatSidenavModule,
        MatInputModule,
        MatButtonModule,
        ReactiveFormsModule
    ],
  providers: [
    SideNavService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
