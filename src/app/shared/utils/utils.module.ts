import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ChartsModule} from 'ng2-charts';
import {MatToolbarModule} from '@angular/material/toolbar';
import {ChartsComponent} from "./charts/charts.component";
import {MatIconModule} from '@angular/material/icon';
import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FlexModule} from '@angular/flex-layout';



@NgModule({
  declarations: [
    ChartsComponent,
    AlertDialogComponent
  ],
  imports: [
    CommonModule,
    ChartsModule,
    MatToolbarModule,
    MatIconModule,
    MatDialogModule,
    FlexModule
  ],
  exports: [
    ChartsComponent,
    AlertDialogComponent
  ],
  entryComponents: [AlertDialogComponent]
})
export class UtilsModule { }
