import {EventEmitter, Output} from '@angular/core';
import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-alert-dialog',
  templateUrl: './alert-dialog.component.html',
  styleUrls: ['./alert-dialog.component.scss']
})
export class AlertDialogComponent implements OnInit {

  labelAnswer = '';
  type = 'confirm'
  @Output() responseAcceptReject = new EventEmitter<boolean>();
  constructor( public dialogRef: MatDialogRef<AlertDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.labelAnswer = data.labelAnswer;
    this.type = data.type;
  }

  ngOnInit(): void {
  }

  close(){
    this.dialogRef.close();
  }

  confirmacionAlerta(action: boolean){
    this.responseAcceptReject.emit(action);
    this.dialogRef.close();
  }


}
