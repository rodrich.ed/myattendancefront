import { Role } from "@core/guards/roles";

export const navigation = [
  {
    "id": "announcements",
    "name": "Dashboard  ",
    "icons": "fas fa-table",
    "role": "",
    "children": [
      {
        "id": "blogs",
        "name": "Panel de control",
        "icons": "",
        "role": "",
        "children": [
          {
            "id": "blogs-r",
            "name": "RR.HH",
            "url": "/v1/alumnos",
            "tags": "",
            "role": [Role.Coordinador, Role.Supervisor, Role.Administrador],
            "extUsers": [113, 121, 93]
          },
          {
            "id": "base-state",
            "name": "Marketing",
            "url": "/v1/reportes",
            "tags": "",

          }
        ]
      },

    ]
  },
  // {
  //   "id": "settings",
  //   "name": "Configuraciones",
  //   "icons": "fa fa-cog",
  //   "role": "",
  //   "children": [
  //     {
  //       "id": "goals-assign",
  //       "name": "Asignacion Metas",
  //       "icons": "",
  //       "role": "",
  //       "children": [
  //         {
  //           "id": "base-state",
  //           "name": "Asignacion de Metas",
  //           "tags": "",
  //           "url": "/v1/configuraciones/asigancion-metas",
  //           "role": [Role.Supervisor],
  //           "extUsers": []
  //         }
  //       ]
  //     }
  //   ]
  // }
]
