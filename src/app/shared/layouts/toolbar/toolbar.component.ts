import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {SideNavService} from '@core/layout-services/side-nav.service';
import { IUser } from '@core/services/models';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  opened = false;
  toggleActive:boolean = false;
  @Output() openCloseSideNav:EventEmitter<boolean> = new EventEmitter<boolean>();
  currentUser: IUser = JSON.parse(<string>localStorage.getItem('userObj'));
  constructor(private sidenav: SideNavService) { }

  ngOnInit(): void {

  }

  changeOpe() {
    this.toggleActive = !this.toggleActive;
    this.sidenav.toggle();
  }

  goCRM() {
    window.open('http://ingeniumefp.net/crmbeta2/index.php?module=Potentials&view=List&app=MARKETING', "_blank");
  }
}
