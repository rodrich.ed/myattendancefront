import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SecurityComponent} from './security/security.component';
import {MainComponent} from "./main/main.component";
import {AttendanceComponent} from "./attendance/attendance.component";

const routes: Routes = [
  {
    path: 'v1',
    loadChildren: () => import('./main/main.module').then(m => m.MainModule)
  },
  {path: 'asistencia',component: AttendanceComponent},
  {
    path: 'error',
    loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)
  },

  {path: '**', redirectTo: 'v1'}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
